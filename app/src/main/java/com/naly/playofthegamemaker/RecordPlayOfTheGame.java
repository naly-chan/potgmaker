package com.naly.playofthegamemaker;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.google.firebase.crash.FirebaseCrash;

import org.w3c.dom.Text;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

public class RecordPlayOfTheGame extends AppCompatActivity {
    private static final int VIDEO_CAPTURE = 101, CHOOSE_VIDEO = 102;
    Button backButton, recordIntroButton, recordPlayButton, createPlayOfTheGame;
    CheckBox introCheckBox, playCheckBox;
    TextView createPlayWarningTV;
    EditText playerNameET, characterNameET;

    File tempIntroVideo, tempPlayVideo;

    // This variable is used to determine which video was being recorded: 0 for null, 1 for intro, 2 for play
    int recording = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recordplay);
        verifyStoragePermissions(this);
        if(deleteCacheDir()){ Log.i("meow", "Cleared cache!");} else { Log.i("meow", "Unsuccessful in clearing cache!");}

        // Set typeface
        Typeface custom_font = Typeface.createFromAsset(getAssets(),  "big_noodle_titling_oblique.ttf");
        TextView playerTV = (TextView) findViewById(R.id.playerNameText);
        playerTV.setTypeface(custom_font);
        TextView characterTV = (TextView) findViewById(R.id.characterNameText);
        characterTV.setTypeface(custom_font);
        TextView introCheckboxTV = (TextView) findViewById(R.id.introCheckboxText);
        introCheckboxTV.setTypeface(custom_font);
        TextView potgTV = (TextView) findViewById(R.id.potgCheckBoxText);
        potgTV.setTypeface(custom_font);
        TextView warningTV = (TextView) findViewById(R.id.warningText);
        warningTV.setTypeface(custom_font);
        TextView instructionsTV = (TextView) findViewById(R.id.instructionsText);
        instructionsTV.setTypeface(custom_font);
        TextView titleRecordTV = (TextView) findViewById(R.id.titleRecord);
        titleRecordTV.setTypeface(custom_font);
        createPlayWarningTV = (TextView) findViewById(R.id.createPlayNotice);
        createPlayWarningTV.setTypeface(custom_font);

        playerNameET = ((EditText) findViewById(R.id.playerNameEditText));
        characterNameET = ((EditText) findViewById(R.id.characterNameEditText));

        introCheckBox = (CheckBox) findViewById(R.id.introCheckBox);
        playCheckBox = (CheckBox) findViewById(R.id.playCheckBox);

        recordIntroButton = (Button) findViewById(R.id.recordIntroButton);
        recordIntroButton.setTag(0);
        recordIntroButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                recording = 1;
                createPlayWarningTV.setVisibility(View.GONE);
                final int status = (Integer) v.getTag();
                tempIntroVideo.delete();
                tempIntroVideo = new File(getExternalCacheDir(), "/introVid.mp4");
                captureVideo(tempIntroVideo, (status == 0) ? true : false);
            }
        });
        recordIntroButton.setTypeface(custom_font);

        recordPlayButton = (Button) findViewById(R.id.recordPlayButton);
        recordPlayButton.setTag(0);
        recordPlayButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                recording = 2;
                createPlayWarningTV.setVisibility(View.GONE);
                final int status = (Integer) v.getTag();
                tempPlayVideo.delete();
                tempPlayVideo = new File(getExternalCacheDir(), "/playVid.mp4");
                captureVideo(tempPlayVideo, (status == 0) ? true : false);
            }
        });
        recordPlayButton.setTypeface(custom_font);

        TextView rotationFix = (TextView) findViewById(R.id.rotationFixText);
        rotationFix.setTypeface(custom_font);

        ToggleButton toggleRotationFix = (ToggleButton) findViewById(R.id.rotationFixToggle);
        toggleRotationFix.setTypeface(custom_font);

        ToggleButton toggle = (ToggleButton) findViewById(R.id.toggleRecordingMode);
        toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // The toggle is enabled
                    recordIntroButton.setText("Choose");
                    recordIntroButton.setTag(1);
                    recordPlayButton.setText("Choose");
                    recordPlayButton.setTag(1);

                } else {
                    // The toggle is disabled
                    recordIntroButton.setText("Record");
                    recordIntroButton.setTag(0);
                    recordPlayButton.setText("Record");
                    recordPlayButton.setTag(0);
                }
            }
        });

        createPlayOfTheGame = (Button) findViewById(R.id.createPlayOfTheGameButton);
        createPlayOfTheGame.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                if(tempIntroVideo.exists() && tempPlayVideo.exists() &&
                        !TextUtils.isEmpty(playerNameET.getText()) && !TextUtils.isEmpty(characterNameET.getText())){
                    Intent intent = new Intent(RecordPlayOfTheGame.this, progressScreen.class);
                    intent.putExtra("tempIntroPath", tempIntroVideo.getAbsolutePath());
                    intent.putExtra("tempPlayPath", tempPlayVideo.getAbsolutePath());
                    intent.putExtra("playerName", playerNameET.getText().toString());
                    intent.putExtra("characterName", characterNameET.getText().toString());
                    ToggleButton toggleRotationFix = (ToggleButton) findViewById(R.id.rotationFixToggle);
                    intent.putExtra("rotationFix", toggleRotationFix.isChecked());
                    startActivity(intent);
                } else {
                    createPlayWarningTV.setVisibility(View.VISIBLE);
                    if(!tempIntroVideo.exists() || !tempPlayVideo.exists()){
                        createPlayWarningTV.setText("Please make sure intro and play videos have been recorded.");
                    } else{
                        //The edit text is or are empty
                        createPlayWarningTV.setText("Please make sure all text fields are filled.");
                    }
                }
            }
        });
        createPlayOfTheGame.setTypeface(custom_font);

        backButton = (Button) findViewById(R.id.ReturnToMainMenu);
        backButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(RecordPlayOfTheGame.this, MainActivity.class);
                startActivity(intent);
            }
        });
        backButton.setTypeface(custom_font);

        assureVideoBlank();
    }

    private void assureVideoBlank(){
        //try {
            tempIntroVideo = new File(getExternalCacheDir(), "/introVid.mp4");
            if(tempIntroVideo.exists()){
                tempIntroVideo.delete();
            }

            tempPlayVideo = new File(getExternalCacheDir(), "/playVid.mp4");
            if(tempPlayVideo.exists()){
                tempPlayVideo.delete();
            }
        /*} catch(Exception e){
            FirebaseCrash.log("assureVideoBlank crash: " + e.toString());
        }*/
    }

    private void captureVideo(File vidFile, boolean videoCapture){
        //try {
        Intent videoIntent;
        Uri videoUri = Uri.fromFile(vidFile);
        if(videoCapture){
            videoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
            videoIntent.putExtra(MediaStore.EXTRA_OUTPUT, videoUri);
        } else {
            videoIntent = new Intent(Intent.ACTION_GET_CONTENT);
            videoIntent.setType("video/*");
            videoIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
            videoIntent.createChooser(videoIntent, "Choose a video");
            //videoIntent.putExtra(MediaStore.EXTRA_OUTPUT, videoUri);
        }

        //takeVideoIntent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, maxDuration);
        if (videoIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(videoIntent, (videoCapture) ? VIDEO_CAPTURE : CHOOSE_VIDEO);
        }
        /*} catch(Exception e){
            FirebaseCrash.log("captureVideo crash: " + e.toString());
        }*/
    }

    /*private void chooseVideoFile(File vidFile){
        Uri videoUri = Uri.fromFile(vidFile);
        mediaChooser.putExtra(MediaStore.EXTRA_OUTPUT, videoUri);
        startActivityForResult(mediaChooser, CHOOSE_VIDEO);
    }*/

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //try {
            if (resultCode == RESULT_OK && data != null) {
                if (requestCode == VIDEO_CAPTURE) {
                    Log.i("meow", "meow created " + data.getData());
                } else if (requestCode == CHOOSE_VIDEO) {
                    Log.i("meow", "meow choose " + data.getData().getPath());
                    if (recording == 1) {
                        try {
                            //tempIntroVideo.createNewFile();
                            copyFile(new File(getRealPathFromURI(data.getData())), tempIntroVideo);
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    } else if (recording == 2) {
                        //tempPlayVideo = new File(data.getData().getPath());

                        try {
                            //tempPlayVideo.createNewFile();
                            copyFile(new File(getRealPathFromURI(data.getData())), tempPlayVideo);
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }
                }
                // Set checkbox to true according to the recorded vid
                if (recording == 1) {
                    introCheckBox.setChecked(true);
                } else if (recording == 2) {
                    playCheckBox.setChecked(true);
                }

            } else if (resultCode == RESULT_CANCELED) {
                Log.i("meow", "Cancelled.");
            } else {
                Log.i("meow", "Error recording video");
            }
            if (introCheckBox.isChecked() && playCheckBox.isChecked()) {
                createPlayOfTheGame.setEnabled(true);
                createPlayOfTheGame.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
            }
            recording = 0;
        /*} catch(Exception e){
            FirebaseCrash.log("onActivityResult crash: " + e.toString());
        }*/
    }

    // Storage Permissions variables
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    // permission method
    public static void verifyStoragePermissions(Activity activity) {
        //try {
            // Check if we have read or write permission
            int writePermission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            int readPermission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE);

            if (writePermission != PackageManager.PERMISSION_GRANTED || readPermission != PackageManager.PERMISSION_GRANTED) {
                // We don't have permission so prompt the user
                ActivityCompat.requestPermissions(
                        activity,
                        PERMISSIONS_STORAGE,
                        REQUEST_EXTERNAL_STORAGE
                );
            }
        /*} catch (Exception e){
            FirebaseCrash.log("verifyStoragePermissions crash: " + e.toString());
        }*/
    }

    private boolean deleteCacheDir()
    {
        File dir = getExternalCacheDir();
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = (new File(dir, children[i])).delete();
                if (!success) {
                    return false;
                }
            }
        }
        return true;
    }

    private void copyFile(File sourceFile, File destFile) throws IOException {
        Log.i("meow", "copying");
        if (!sourceFile.exists()) {
            Log.i("meow", "source file doesnt exist " + sourceFile.getAbsolutePath());
            return;
        }

        FileChannel source = null;
        FileChannel destination = null;
        source = new FileInputStream(sourceFile).getChannel();
        destination = new FileOutputStream(destFile).getChannel();
        if (destination != null && source != null) {
            destination.transferFrom(source, 0, source.size());
        }
        if (source != null) {
            source.close();
        }
        if (destination != null) {
            destination.close();
        }


    }

    private String getRealPathFromURI(Uri contentUri) {
        return PathHelper.getPath(this, contentUri);
    }
}

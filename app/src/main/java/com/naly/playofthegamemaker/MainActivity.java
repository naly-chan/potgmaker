package com.naly.playofthegamemaker;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

public class MainActivity extends AppCompatActivity {
    Button startRecordingButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Ads
        MobileAds.initialize(getApplicationContext(), "ca-app-pub-6435649048408849~1452473016");
        final AdView mAdView = (AdView) findViewById(R.id.mainAdView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        // Set typeface
        Typeface custom_font = Typeface.createFromAsset(getAssets(),  "big_noodle_titling_oblique.ttf");
        TextView tv = (TextView) findViewById(R.id.TextViewMain);
        tv.setTypeface(custom_font);

        startRecordingButton = (Button) findViewById(R.id.StartRecording);
        startRecordingButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mAdView.destroy();
                Intent intent = new Intent(MainActivity.this, RecordPlayOfTheGame.class);
                startActivity(intent);
            }
        });

        startRecordingButton.setTypeface(custom_font);

        TextView textView =(TextView)findViewById(R.id.policy);
        textView.setClickable(true);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        String text = "<a href='https://raw.githubusercontent.com/UnlimitedAmbitions/documents/70cb1be2adc0950f0c9e4d1ac65439465b3fbb9d/mobile-privacy-policy-ad-sponsored-apps-february-07-2017.html'> Read Privacy Policies </a>";
        textView.setText(Html.fromHtml(text));

    }
}

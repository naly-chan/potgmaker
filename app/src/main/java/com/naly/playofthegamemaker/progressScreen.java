package com.naly.playofthegamemaker;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.Typeface;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.hiteshsondhi88.libffmpeg.ExecuteBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.LoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Naly on 2016-07-11.
 */
public class progressScreen extends AppCompatActivity {
    Button confirmSuccessfulConversion, cancelProcessingButton, shareVideoButton, openFileLocationButton;
    File tempIntroVideo, tempPlayVideo, processedIntro, processedPlay, processedMpgIntro, processedAudioPlay,
            processedMpgPlay, processedAudioIntro, finalStorage, playofthegame, playmusic, font;
    String finalFileLocation = null, playerName = null, characterName = null;
    TextView statusText, pathText, processingText;
    Thread processing;
    int progressInc = 1;

    Boolean doRotationFix;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        initialiseFFMPEGLibrary();

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss") ;
        finalStorage = getDCIMDirectory();
        processedIntro = (new File(getExternalCacheDir(), "/tempIntro.mp4"));
        processedIntro.deleteOnExit();
        processedPlay = (new File(getExternalCacheDir(), "/tempPlay.mp4"));
        processedPlay.deleteOnExit();
        processedMpgIntro = (new File(getExternalCacheDir(), "/tempIntrom.mpg"));
        processedMpgIntro.deleteOnExit();
        processedMpgPlay = (new File(getExternalCacheDir(), "/tempPlaym.mpg"));
        processedMpgPlay.deleteOnExit();
        processedAudioIntro = (new File(getExternalCacheDir(), "/tempAudioIntro.mp4"));
        processedAudioIntro.deleteOnExit();
        processedAudioPlay = (new File(getExternalCacheDir(), "/tempAudioPlay.mp4"));
        processedAudioPlay.deleteOnExit();
        finalFileLocation = ((new File(finalStorage, "potg_" + dateFormat.format(Calendar.getInstance().getTime()) + ".mp4").getAbsolutePath()));
        Log.i("meow", "final file location: " + finalFileLocation);
        // Create temp files for music and font
        try{
            //we need to create temp version of mp3 and font
            InputStream rawSound = getAssets().open("play_of_the_game.aac");
            playofthegame = new File(getExternalCacheDir(), "/playofthegame.aac");
            copyInputStreamToFile(rawSound, playofthegame);

            InputStream rawSound2 = getAssets().open("play_music.aac");
            playmusic = new File(getExternalCacheDir(), "/playmusic.aac");
            copyInputStreamToFile(rawSound2, playmusic);

            InputStream rawFont = getAssets().open("big_noodle_titling_oblique.ttf");
            font = new File(getExternalCacheDir(), "/font.ttf");
            copyInputStreamToFile(rawFont, font);
        } catch(IOException ex){
            ex.printStackTrace();
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_progressscreen);
        Typeface custom_font = Typeface.createFromAsset(getAssets(),  "big_noodle_titling_oblique.ttf");
        TextView pleasewaitTV = (TextView) findViewById(R.id.pleasewait);
        pleasewaitTV.setTypeface(custom_font);
        statusText = (TextView) findViewById(R.id.status);
        statusText.setTypeface(custom_font);
        TextView completedTV = (TextView) findViewById(R.id.completedText);
        completedTV.setTypeface(custom_font);
        pathText = (TextView) findViewById(R.id.pathText);
        pathText.setTypeface(custom_font);
        processingText = (TextView) findViewById(R.id.processingProgress);
        processingText.setTypeface(custom_font);

        // Ads
        MobileAds.initialize(getApplicationContext(), "ca-app-pub-6435649048408849~1452473016");
        final AdView mAdView = (AdView) findViewById(R.id.processAdView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        // Retrieve the recorded temp videos
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            tempIntroVideo = new File(extras.getString("tempIntroPath"));
            tempPlayVideo = new File(extras.getString("tempPlayPath"));
            playerName = extras.getString("playerName");
            characterName = extras.getString("characterName");
            doRotationFix = extras.getBoolean("rotationFix");
        }


        confirmSuccessfulConversion = (Button) findViewById(R.id.confirmSuccessfulConversion);
        confirmSuccessfulConversion.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                mAdView.destroy();
                Intent intent = new Intent(progressScreen.this, MainActivity.class);
                startActivity(intent);
            }
        });
        confirmSuccessfulConversion.setTypeface(custom_font);

        openFileLocationButton = (Button) findViewById(R.id.openFileLocation);
        openFileLocationButton.setTypeface(custom_font);



        shareVideoButton = (Button) findViewById(R.id.shareVideoButton);
        shareVideoButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Log.i("meow", "Scanning file...");
                Intent shareIntent = new Intent(
                        Intent.ACTION_SEND);
                shareIntent.setType("video/mp4");
                shareIntent.putExtra(
                        android.content.Intent.EXTRA_SUBJECT, "My play of the game");
                shareIntent.putExtra(
                        android.content.Intent.EXTRA_TITLE, "My play of the game");
                shareIntent.putExtra(
                        Intent.EXTRA_TEXT, "Created with Play of the Game Maker!");
                shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(new File(finalFileLocation)));
                Log.i("meow", "Start share intent Activity...");
                Intent new_intent = Intent.createChooser(shareIntent, "Share via");
                new_intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getApplicationContext().startActivity(new_intent);
            }
        });
        shareVideoButton.setTypeface(custom_font);

        /*cancelProcessingButton = (Button) findViewById(R.id.cancelProcessing);
        cancelProcessingButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                AlertDialog alertDialog = new AlertDialog.Builder(progressScreen.this).create();
                alertDialog.setTitle("Cancel video processing...");
                alertDialog.setMessage("Are you sure about stopping the video processing?");
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                processing.interrupt();
                                Intent intent = new Intent(progressScreen.this, MainActivity.class);
                                startActivity(intent);
                            }
                        });
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
            }
        });
        cancelProcessingButton.setTypeface(custom_font);*/

        processing = new Thread(){
            public void run(){
                createPlayOfTheGame();
            }
        };
        processing.start();
    }

    private void createPlayOfTheGame(){
        executeFirstCommand();
    }

    private void copyInputStreamToFile(InputStream in, File file ) {
        try {
            OutputStream out = new FileOutputStream(file);
            byte[] buf = new byte[1024];
            int len;
            while((len=in.read(buf))>0){
                out.write(buf,0,len);
            }
            out.close();
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void executeFirstCommand(){
        statusText.setText("Adding text... (1/3)");

        MediaMetadataRetriever m = new MediaMetadataRetriever();
        m.setDataSource(tempIntroVideo.getAbsolutePath());
        Bitmap thumbnail = m.getFrameAtTime();
        String s = m.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_ROTATION);
        Log.i("Rotation tempIntroVideo", s);

        // Calculate font size
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int smallest = (size.y > size.x) ? size.x : size.y;
        int font1 = (int)(90*smallest/1440), font2 = (int)(140*smallest/1440), font3 = (int)(70*smallest/1440);

        FFmpeg ffmpeg = FFmpeg.getInstance(getApplicationContext());

        String[] cmds = {
                "-noautorotate","-i", tempIntroVideo.getAbsolutePath(),  "-i", playofthegame.getAbsolutePath(), "-vf",
                "drawtext=fontsize="+font1+":fontcolor=white:fontfile="+font+":text='play of the game':x=(200*w/1920):y=(h/4),drawtext=fontsize="+font2+":fontcolor=yellow:fontfile="+font+":text='" + center(playerName, 20) + "':x=(250*w/1920):y=(2*h/5),drawtext=fontsize="+font3+":fontcolor=white:fontfile="+font+":text='as " + characterName + "':x=(300*w/1920):y=(h/7*4.2)",
                processedIntro.getAbsolutePath()};

        String[] Rcmds = {
                "-i", tempIntroVideo.getAbsolutePath(),  "-i", playofthegame.getAbsolutePath(), "-vf",
                "drawtext=fontsize="+font1+":fontcolor=white:fontfile="+font+":text='play of the game':x=(200*w/1920):y=(h/4),drawtext=fontsize="+font2+":fontcolor=yellow:fontfile="+font+":text='" + center(playerName, 20) + "':x=(250*w/1920):y=(2*h/5),drawtext=fontsize="+font3+":fontcolor=white:fontfile="+font+":text='as " + characterName + "':x=(300*w/1920):y=(h/7*4.2)",
                processedIntro.getAbsolutePath()};
                progressInc = 1;

        try {
            // to execute "ffmpeg -version" command you just need to pass "-version"
            ffmpeg.execute((doRotationFix) ? Rcmds : cmds, new ExecuteBinaryResponseHandler() {
                @Override
                public void onStart() {
                    Log.i("meow"," starting...");
                }

                @Override
                public void onProgress(String message) {
                    Log.i("meow","progress:" + message);
                    processingText.setText("Processing frame " + progressInc);
                    progressInc++;
                }

                @Override
                public void onFailure(String message) {
                    Log.i("meow","fail" + message);
                }

                @Override
                public void onSuccess(String message) {
                    Log.i("meow","success" + message);
                }

                @Override
                public void onFinish() {
                    Log.i("meow", "finished 1st command");

                    executeFirstAndHalfCommand();
                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
            // Handle if FFmpeg is already running
            e.printStackTrace();
        }
    }

    private void executeFirstAndHalfCommand(){
        FFmpeg ffmpeg = FFmpeg.getInstance(getApplicationContext());

        MediaMetadataRetriever m = new MediaMetadataRetriever();
        m.setDataSource(processedIntro.getAbsolutePath());
        Bitmap thumbnail = m.getFrameAtTime();
        String s = m.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_ROTATION);
        Log.i("Rotation processedIntro", s);

        String[] cmds = {
                "-noautorotate", "-i", processedIntro.getAbsolutePath(),"-i", playofthegame.getAbsolutePath(), "-map", "0:v",
                "-map", "1:a", "-c", "copy", "-shortest", "-bsf:a", "aac_adtstoasc",
                processedAudioIntro.getAbsolutePath()};

        String[] Rcmds = {
                "-i", processedIntro.getAbsolutePath(),"-i", playofthegame.getAbsolutePath(), "-map", "0:v",
                "-map", "1:a", "-c", "copy", "-shortest", "-bsf:a", "aac_adtstoasc",
                processedAudioIntro.getAbsolutePath()};

        try {
            // to execute "ffmpeg -version" command you just need to pass "-version"
            ffmpeg.execute((doRotationFix) ? Rcmds : cmds, new ExecuteBinaryResponseHandler() {

                @Override
                public void onStart() {
                    Log.i("meow"," starting...");
                }

                @Override
                public void onProgress(String message) {
                    Log.i("meow","progress:" + message);
                }

                @Override
                public void onFailure(String message) {
                    Log.i("meow","fail" + message);
                }

                @Override
                public void onSuccess(String message) {
                    Log.i("meow","success" + message);
                }

                @Override
                public void onFinish() {
                    Log.i("meow", "finished 1.5th command");
                    executeSecondCommand();
                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
            // Handle if FFmpeg is already running
            e.printStackTrace();
        }
    }

    private void executeSecondCommand(){
        FFmpeg ffmpeg = FFmpeg.getInstance(getApplicationContext());

        String[] cmds = {
                "-i", processedAudioIntro.getAbsolutePath(),
                "-c", "copy", "-shortest", "-bsf:v", "h264_mp4toannexb", "-f", "mpegts",
                processedMpgIntro.getAbsolutePath()};

        MediaMetadataRetriever m = new MediaMetadataRetriever();
        m.setDataSource(processedIntro.getAbsolutePath());
        Bitmap thumbnail = m.getFrameAtTime();
        String s = m.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_ROTATION);
        Log.i("Rotation processedIntro", s);


        try {
            // to execute "ffmpeg -version" command you just need to pass "-version"
            ffmpeg.execute(cmds, new ExecuteBinaryResponseHandler() {
                int progressInc = 0;
                @Override
                public void onStart() {
                    Log.i("meow"," starting...");
                }

                @Override
                public void onProgress(String message) {
                    Log.i("meow","progress:" + message);
                    processingText.setText("Processing frame " + progressInc);
                    progressInc++;
                }

                @Override
                public void onFailure(String message) {
                    Log.i("meow","fail" + message);
                }

                @Override
                public void onSuccess(String message) {
                    Log.i("meow","success" + message);
                }

                @Override
                public void onFinish() {
                    Log.i("meow", "finished 2nd command");
                    //statusText.setText("Processing intro video (adding music)... (1/3)");
                    executeThirdCommand();
                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
            // Handle if FFmpeg is already running
            e.printStackTrace();
        }
    }

    private void executeThirdCommand(){
        statusText.setText("Adding music to play... (2/3)");

        if(!doRotationFix){
            executeThirdAndHalfCommand();
            return;
        }

        FFmpeg ffmpeg = FFmpeg.getInstance(getApplicationContext());

        String[] cmds = {
                "-i", tempPlayVideo.getAbsolutePath(), "-vf",
                "drawtext=enable=0:fontsize=90:fontcolor=white:fontfile="+font+":text=' ':x=(200):y=(h/4)",
                processedPlay.getAbsolutePath()};
        progressInc = 1;
        try {
            // to execute "ffmpeg -version" command you just need to pass "-version"
            ffmpeg.execute(cmds, new ExecuteBinaryResponseHandler() {
                @Override
                public void onStart() {
                    Log.i("meow"," starting...");
                }

                @Override
                public void onProgress(String message) {
                    Log.i("meow","progress:" + message);
                    processingText.setText("Processing frame " + progressInc);
                    progressInc++;
                }

                @Override
                public void onFailure(String message) {
                    Log.i("meow","fail" + message);
                }

                @Override
                public void onSuccess(String message) {
                    Log.i("meow","success" + message);
                }

                @Override
                public void onFinish() {
                    Log.i("meow", "finished 3rd command");
                    executeThirdAndHalfCommand();
                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
            // Handle if FFmpeg is already running
            e.printStackTrace();
        }
    }

    private void executeThirdAndHalfCommand(){
        FFmpeg ffmpeg = FFmpeg.getInstance(getApplicationContext());

        String[] cmds = {
                "-i", tempPlayVideo.getAbsolutePath(),  "-i", playmusic.getAbsolutePath(), "-map", "0:v",
                "-map", "1:a", "-c", "copy", "-shortest", "-bsf:a", "aac_adtstoasc",
                processedAudioPlay.getAbsolutePath()};
        String[] Rcmds = {
                "-i", processedPlay.getAbsolutePath(),  "-i", playmusic.getAbsolutePath(), "-map", "0:v",
                "-map", "1:a", "-c", "copy", "-shortest", "-bsf:a", "aac_adtstoasc",
                processedAudioPlay.getAbsolutePath()};

        MediaMetadataRetriever m = new MediaMetadataRetriever();
        m.setDataSource(tempPlayVideo.getAbsolutePath());
        Bitmap thumbnail = m.getFrameAtTime();
        String s = m.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_ROTATION);
        Log.i("Rotation tempPlayVideo", s);
        progressInc = 1;

        try {
            // to execute "ffmpeg -version" command you just need to pass "-version"
            ffmpeg.execute((doRotationFix) ? Rcmds:cmds, new ExecuteBinaryResponseHandler() {
                int progressInc = 0;
                @Override
                public void onStart() {
                    Log.i("meow"," starting...");
                }

                @Override
                public void onProgress(String message) {
                    Log.i("meow","progress:" + message);
                    processingText.setText("Processing frame " + progressInc);
                    progressInc++;
                }

                @Override
                public void onFailure(String message) {
                    Log.i("meow","fail" + message);
                }

                @Override
                public void onSuccess(String message) {
                    Log.i("meow","success" + message);
                }

                @Override
                public void onFinish() {
                    Log.i("meow", "finished 3th command");
                    executeFourthCommand();
                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
            // Handle if FFmpeg is already running
            e.printStackTrace();
        }
    }

    private void executeFourthCommand(){
        FFmpeg ffmpeg = FFmpeg.getInstance(getApplicationContext());

        MediaMetadataRetriever m = new MediaMetadataRetriever();
        m.setDataSource(processedAudioPlay.getAbsolutePath());
        Bitmap thumbnail = m.getFrameAtTime();
        String rot = m.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_ROTATION);
        Log.i("Rotation AudioPlay", rot);

        String[] cmds = {
                "-i", processedAudioPlay.getAbsolutePath(),//  "-vf",
                // "drawtext=fontsize=90:fontcolor=white:fontfile="+font+":text='play of the game':x=(200):y=(h/4),drawtext=fontsize=140:fontcolor=yellow:fontfile="+font+":text='" + center(playerName, 20) + "':x=(175):y=(h/3),drawtext=fontsize=70:fontcolor=white:fontfile="+font+":text='as " + center(characterName, 20) + "':x=(300):y=(h/2-text_h*3/2)",
                "-c", "copy", "-shortest", "-bsf:v", "h264_mp4toannexb", "-f", "mpegts",
                processedMpgPlay.getAbsolutePath()};



        try {
            // to execute "ffmpeg -version" command you just need to pass "-version"
            ffmpeg.execute(cmds, new ExecuteBinaryResponseHandler() {

                @Override
                public void onStart() {
                    Log.i("meow"," starting...");
                }

                @Override
                public void onProgress(String message) {
                    Log.i("meow","progress:" + message);
                    processingText.setText("Processing frame " + progressInc);
                    progressInc++;
                }

                @Override
                public void onFailure(String message) {
                    Log.i("meow","fail" + message);
                }

                @Override
                public void onSuccess(String message) {
                    Log.i("meow","success" + message);
                }

                @Override
                public void onFinish() {
                    Log.i("meow", "finished 4th command");
                    //statusText.setText("Processing intro video (adding music)... (1/3)");
                    executeLastCommand();
                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
            // Handle if FFmpeg is already running
            e.printStackTrace();
        }
    }

    private void executeLastCommand(){
        FFmpeg ffmpeg = FFmpeg.getInstance(getApplicationContext());
        statusText.setText("Finalizing play of the game video... (3/3)");
        progressInc = 1;
        processingText.setVisibility(View.GONE);

        String[] cmds = {
                "-i", "concat:" +  processedMpgIntro.getAbsolutePath() + "|" + processedMpgPlay.getAbsolutePath(), "-c", "copy", "-bsf:a", "aac_adtstoasc", finalFileLocation};

        MediaMetadataRetriever m = new MediaMetadataRetriever();
        m.setDataSource(processedMpgIntro.getAbsolutePath());
        Bitmap thumbnail = m.getFrameAtTime();
        String s = m.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_ROTATION);
        Log.i("Rotation MpgIntro", s);

        m.setDataSource(processedMpgPlay.getAbsolutePath());
        thumbnail = m.getFrameAtTime();
        s = m.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_ROTATION);
        Log.i("Rotation MpgPlay", s);

        try {
            // to execute "ffmpeg -version" command you just need to pass "-version"
            ffmpeg.execute(cmds, new ExecuteBinaryResponseHandler() {

                @Override
                public void onStart() {
                    Log.i("meow","grr starting...");
                }

                @Override
                public void onProgress(String message) {
                    Log.i("meow","grr" + message);
                }

                @Override
                public void onFailure(String message) {
                    Log.i("meow","grr" + message);
                }

                @Override
                public void onSuccess(String message) {
                    Log.i("meow","grr" + message);
                }

                @Override
                public void onFinish() {

                    MediaMetadataRetriever m = new MediaMetadataRetriever();
                    m.setDataSource(finalFileLocation);
                    Bitmap thumbnail = m.getFrameAtTime();
                    String s = m.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_ROTATION);
                    Log.i("Rotation final", s);


                    Log.i("meow","Finished!");
                    pathText.setText("Your new play is located at " + finalFileLocation + "!");
                    tempIntroVideo.delete();
                    tempPlayVideo.delete();
                    processedIntro.delete();
                    //processedMpgIntro.delete();
                    processedPlay.delete();
                    //processedMpgPlay.delete();
                    processedAudioIntro.delete();
                    processedAudioPlay.delete();
                    playofthegame.delete();
                    playmusic.delete();
                    font.delete();

                    // open file location
                    openFileLocationButton.setOnClickListener(new View.OnClickListener(){
                        public void onClick(View v){
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            Uri uri = Uri.parse(finalFileLocation);
                            Log.i("meow", "filelocation: " + uri.getPath());
                            intent.setDataAndType(uri, "video/*");
                            startActivity(intent);
                        }
                    });

                    LinearLayout waitingBlock = (LinearLayout) findViewById(R.id.waitingBlock);
                    waitingBlock.setVisibility(View.GONE);
                    LinearLayout processingDoneBlock = (LinearLayout) findViewById(R.id.processingDoneBlock);
                    processingDoneBlock.setVisibility(View.VISIBLE);
                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
            // Handle if FFmpeg is already running
            e.printStackTrace();
        }
    }

    private void initialiseFFMPEGLibrary(){
        FFmpeg ffmpeg = FFmpeg.getInstance(this.getApplicationContext());
        try {
            ffmpeg.loadBinary(new LoadBinaryResponseHandler() {

                @Override
                public void onStart() {}

                @Override
                public void onFailure() {}

                @Override
                public void onSuccess() {}

                @Override
                public void onFinish() {}
            });
        } catch (FFmpegNotSupportedException e) {
            // Handle if FFmpeg is not supported by device
        }
    }

    public static String center(String text, int len){
        String out = String.format("%"+len+"s%s%"+len+"s", "",text,"");
        float mid = (out.length()/2);
        float start = mid - (len/2);
        float end = start + len;
        return out.substring((int)start, (int)end);
    }

    private File getDCIMDirectory() {
        // Get the directory for the user's public pictures directory.
        File file = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DCIM), "POTGMaker");
        if (!file.mkdirs()) {
            Log.i("meow", "Directory not created");
        }
        return file;
    }
}
